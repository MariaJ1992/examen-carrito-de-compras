﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarritoDeCompras
{
    public class StockProductos
    {

        public StockProductos()
        {
            this.ListaStockProductos = new List<Producto>();
        }

        public List<Producto> ListaStockProductos { get; set; }

        public void CrearProductos()
        {
            Random random = new Random();
            int numero;
            int exis;
            for (int i = 1; i <= 10; i++)
            {

                Producto producto = new Producto();
                producto.Identificador = i;
                numero = random.Next(20);
                exis = random.Next(10);
                char letra = (char)(((int)'A') + random.Next(26));
                producto.Descripcion = "PRODUCTO" + letra + numero + i;
                producto.Precio = numero;
                producto.Existencia = exis;
                this.ListaStockProductos.Add(producto);
            }
        }

        public void ImprimirStockProductos()
        {
            Console.WriteLine("Stock de Productos");
            Console.WriteLine("Identificador\tDescripción\tPrecio\tExistencia");
            List<string> Produc = (from producto in ListaStockProductos
                                    orderby producto.Precio ascending
                                   select "\t" + producto.Identificador+ "\t" + producto.Descripcion+ "\t" + producto.Precio+ "\t" +producto.Existencia).ToList();
            Console.WriteLine(string.Join("\n", Produc ));

        }
        public void ProductoPrecioMayor()
        {
            Console.WriteLine("");
            var mayor = ListaStockProductos.Max(x => x.Precio );
            Console.WriteLine("Producto de mayor precio tiene un valor de: " +mayor);
        }

        public void buscar()
        {
            Console.WriteLine("Ingrese el codigo del producto a buscar");
            int codigounico = int.Parse(Console.ReadLine());
            var busqueda = from busqueprodu in ListaStockProductos
                           where busqueprodu.Identificador == codigounico
                           select "Producto a buscar: " + busqueprodu.Descripcion + " Precio: " + busqueprodu.Precio + " Existenes: " +busqueprodu.Existencia;
            Console.WriteLine(string.Join("\n", busqueda));
        }
    }
}
